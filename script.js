/*
	BROKEN VERSION:

	const registerForm = document.querySelector("#registerForm")

	registerForm.addEventListener("Submit", () => {
		e.preventDefault

		const username = document.querySelector("#username").value
		const email = document.querySelector("#email").value
		const mobileNo = document.querySelector("#mobileNum").value
		const password1 = document.querySelector("#password").value
		const password2 = document.querySelector("#password").value

		let condition1 = false
		let condition2 = false
		Let condition3 = false

		if(password1.length < 6){
			alerat("PASSWORD MUST BE AT LEAST 6 CHARACTERS LONG")
		}else{
			condition1 == true
		}

		if(password1.search(/[A-Z]/) < 1) {
			alert("PASSWORD MUST INCLUDE AT LEAST ONE UPPERCASE CHARACTER")
		}else{
			condition2 == true
		}

		if(password1 !== password2){
			alert("PASSWORDS MUST MATCH")
		}else{
			condition3 == true
		}

		if(condition1 === true && condition2 === true $$ condition3 === true){
			alert(`USER ${username} REGISTERED WITH EMAIL {email} AND MOBILE NUMBER ${mobileNo}`)

			username == ""
			email ==  ""
			mobileNo == ""
			password1 == ""
			password2 == ""
		}
	})

	List of Errors:
	1. Line 3 "submit" must be lowercase
	2. Line 3 "e" must be passed to the arguments in the addEventListener method.
	3. Line 4 - () must be added to the end of preventDefault
	4. .value must be removed from ALL querySelectors
	5. Line 8 should get the ID "mobileNo"
	6. Line 9 should get the ID "password1"
	7. Line 10 should get the ID "password2"
	8. Line 14 let should be lowercase
	9. Line 16 should be password1.value.length
	10. Line 17 alerat should be alert
	11. Line 19 should use an assignment operator (single =) instead of loose comparison (double =)
	12. Line 22 should be password1.value.search
	13. Line 22 < 1 should be < 0
	14. Line 25 should use an assignment operator (single =) instead of loose comparison (double =)
	15. Line 28 should be password1.value !== password2.value
	16. Line 31 should use an assignment operator (single =) instead of loose comparison (double =)
	17. Line 34 should use an AND operator (&&) instead of $$
	18. Line 35 should use ${username.value}, ${email.value}, and ${mobileNo.value}
	19. Lines 37 to 41 should all use an assignment operator (single =) instead of loose comparison (double =)
*/

//FIXED VERSION:

const registerForm = document.querySelector("#registerForm")

registerForm.addEventListener("submit", (e) => {
	e.preventDefault()

	const username = document.querySelector("#username")
	const email = document.querySelector("#email")
	const mobileNo = document.querySelector("#mobileNo")
	const password1 = document.querySelector("#password1")
	const password2 = document.querySelector("#password2")

	let condition1 = false
	let condition2 = false
	let condition3 = false

	if(password1.value.length < 6){
		alert("PASSWORD MUST BE AT LEAST 6 CHARACTERS LONG")
		condition1 = false
	}else{
		condition1 = true
	}

	if(password1.value.search(/[A-Z]/) < 0) {
		alert("PASSWORD MUST INCLUDE AT LEAST ONE UPPERCASE CHARACTER")
		condition1 = false
	}else{
		condition2 = true
	}

	if(password1.value !== password2.value){
		alert("PASSWORDS MUST MATCH")
		condition1 = false
	}else{
		condition3 = true
	}

	if(condition1 === true && condition2 === true && condition3 === true){
		alert(`USER ${username.value} REGISTERED WITH EMAIL ${email.value} AND MOBILE NUMBER ${mobileNo.value}`)

		username.value = ""
		email.value =  ""
		mobileNo.value = ""
		password1.value = ""
		password2.value = ""
	}
})